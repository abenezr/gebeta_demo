import { createStore, combineReducers, applyMiddleware } from 'redux';
import {createForms} from 'react-redux-form';
import { Dishes} from './dishes';
import {Books} from './books'
import { Comments} from './comments';
import { Leaders} from './leaders';
import { Promotions} from './promotions';
import {Feedback} from './feedback';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { InitialFeedback } from './forms';

export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            dishes:Dishes,
            books:Books,
            comments:Comments,
            promotions: Promotions,
            leaders: Leaders,
            feedback:Feedback,
            ...createForms({
                feedbacks: InitialFeedback
            })
        }), 
        applyMiddleware(thunk, logger)
    );

    return store;
}