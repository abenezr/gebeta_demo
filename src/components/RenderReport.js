import React, {Component} from "react";
import {CanvasJSChart} from 'canvasjs-react-charts';
import { Col, Row } from "reactstrap";
import { baseUrl } from "../shared/baseUrl";
class RenderReport extends Component {
    render() {
		const options = {
			animationEnabled: true,
			exportEnabled: true,
			theme: "light2", // "light1", "dark1", "dark2"
			title:{
				text: "Completion Rate by Week of Year"
			},
			axisY: {
				title: "Completion Percentage",
				suffix: "%"
			},
			axisX: {
				title: "Week of Year",
				prefix: "W",
				interval: 2
			},
			data: [{
				type: "line",
				toolTipContent: "Week {x}: {y}%",
				dataPoints: [
					{ x: 1, y: 64 },
					{ x: 2, y: 61 },
					{ x: 3, y: 64 },
					{ x: 4, y: 62 },
					{ x: 5, y: 64 },
					{ x: 6, y: 60 },
					{ x: 7, y: 58 },
					{ x: 8, y: 59 },
					{ x: 9, y: 53 },
					{ x: 10, y: 54 },
					{ x: 11, y: 61 },
					{ x: 12, y: 60 },
					{ x: 13, y: 55 },
					{ x: 14, y: 60 },
					{ x: 15, y: 56 },
					{ x: 16, y: 60 },
					{ x: 17, y: 59.5 },
					{ x: 18, y: 63 },
					{ x: 19, y: 58 },
					{ x: 20, y: 54 },
					{ x: 21, y: 59 },
					{ x: 22, y: 64 },
					{ x: 23, y: 59 }
				]
			}]
        }
        const options2 = {
			animationEnabled: true,
			title: {
				text: "Watch Hours"
			},
			subtitles: [{
				text: "",
				verticalAlign: "center",
				fontSize: 24,
				dockInsidePlotArea: true
			}],
			data: [{
				type: "doughnut",
				showInLegend: true,
				indexLabel: "{name}: {y}",
				yValueFormatString: "#,###'%'",
				dataPoints: [
					{ name: "Alien Life", y: 5 },
					{ name: "Automotive", y: 31 },
					{ name: "Books", y: 40 },
					{ name: "Civil Engineering", y: 17 },
					{ name: "Climate Change", y: 7 }
				]
			}]
		}
		return (
		<div>
        {/* <Row>
            <Col md={5} className="reportLogo">
<img src={baseUrl + "images/ahoon.png"}  />
            </Col>
            <Col md={5}> */}
	<CanvasJSChart options = {options}
				/* onRef={ref => this.chart = ref} */
			/>
			<p></p>
    <CanvasJSChart options = {options2}
				/* onRef={ref => this.chart = ref} */
			/>
            {/* </Col>
        </Row>
		 */}
            
			{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
		</div>
		);
	}
}
export default RenderReport; 