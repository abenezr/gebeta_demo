import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card, CardImg, CardBody, CardTitle, BreadcrumbItem, Breadcrumb, Button, Modal, 
  ModalBody, ModalHeader, Label, Row,Col, CardText ,Media} from 'reactstrap';
import { Link } from 'react-router-dom';
// import { play_arrow} from '@material-ui/icons';
import { Control, LocalForm, Errors } from 'react-redux-form';
import  {Loading} from './LoadingComponent';
import {baseUrl} from '../shared/baseUrl';
import ReactPlayer from "react-player"
import { FadeTransform,Fade, Stagger } from 'react-animation-components';
import VideoPlayer from './VideoPlayer';



const minLength = (len) => (val) => !(val) || (val.length >= len);
const maxLength = (len) => (val) => val && (val.length <= len);
const ratingOptions=[1,2,3,4,5,6];
const rating= ratingOptions.map(RenderRatings);

function RenderRatings(num) {

  return (
    <option> {num} </option>
  );
}

class CommentForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isCommentFormOpen: false
    };
    // this.toggleCommentForm = this.toggleCommentForm.bind(this);
  }
  toggleCommentForm() {
    this.setState({
      isCommentFormOpen: !this.state.isCommentFormOpen
    })
  }

  handleSubmit(values) {
    // alert("" + JSON.stringify(values));
    this.props.postComment(this.props.dishId, values.rating,  values.yourname, values.comment);
    this.toggleCommentForm();
  }

  render() {
    
      return (
      
      <div>
        <Button onClick={this.toggleCommentForm} outline color="secondary">
          Submit Comment
                     </Button>

        <Modal isOpen={this.state.isCommentFormOpen} toggle={this.toggleCommentForm}>
          <ModalHeader toggle={this.toggleCommentForm}>
            Comment Form
                    </ModalHeader>
          <ModalBody>
            <div className="container">
              <LocalForm onSubmit={(values => this.handleSubmit(values))}>
                <Row className="form-group">
                  <Label htmlFor="rating">Author</Label>
                  <Control.select model=".rating" id="rating" name="rating"
                    className="form-control" >

                    {rating} 
                    
                  </Control.select>
                </Row>
                <Row className="form-group">
                  <Label htmlFor="yourname">Author</Label>
                  <Control.text model=".yourname" id="yourname" name="yourname"
                    placeholder="Your Name" className="form-control"
                    validators={{ minLength: minLength(3), maxLength: maxLength(15) }} />
                  <Errors
                    model=".yourname"
                    className="text-danger"
                    show='touched'
                    messages={{
                      minLength: "Must be 3 characters or less",
                      maxLength: "Must be 15 characters or more"
                    }}
                  />
                </Row>

                <Row className="form-group">
                  <Label htmlFor="comment">Author</Label>
                  <Control.textarea model=".comment" id="comment" name="comment"
                    placeholder="Place your comment here" className="form-control" />
                </Row>
                <Row className="form-group">
                  <Button type="submit"
                    className="form-group" color="primary" >
                    Submit
                                    </Button>
                </Row>


              </LocalForm>
            </div>
          </ModalBody>
        </Modal>
      </div>

    )
  }
}
class RenderComments extends Component{
  constructor(props) {
    super(props)
    this.state = {
      isVideoPlayerOpen: false,
      urlLink:"jhbj"
    };
    // this.toggleVideoPlayer = this.toggleVideoPlayer.bind(this);
  }

  // toggleVideoPlayer() {
    
  //   this.setState({
  //     isVideoPlayerOpen: !this.state.isVideoPlayerOpen
  //   })
  // }

 


  listComments = this.props.comments.map((commentdetail) => {
    return (
      <Col md={6}>
      
        <Fade in>
        <Link to={`/menu/${commentdetail.dishId}/${encodeURIComponent(commentdetail.videoLink)}`} >
         
      {/* <Card key={commentdetail.id} className="videoList menuItem ">
        <CardImg 
        src={baseUrl + this.props.imagePath }
        />
        <CardBody className="transparent">
          <CardTitle >
{commentdetail.comment}
          </CardTitle>
          <CardText>
{commentdetail.author}
          </CardText>
        </CardBody>
       
      </Card > */}
      <Media key={commentdetail.id} className="videoList menuItem ">
                <Media >
                    <Media object src={baseUrl+this.props.imagePath} 
                                  atl="AhoonGebeta"
                                  className="videolist-media" />
                </Media>
                <Media body className="ml-1">
                    <Media heading>{commentdetail.comment}</Media>
                    {/* <h5>{leader.designation}</h5> */}

                    <p>{commentdetail.author}</p>
                </Media>
            </Media>
     </Link>
      </Fade>
      

     
      </Col>
    );
  }

  );
  render(){
    return (
    <div>
      <h1 className="mt-5 ml-5">Videos</h1>
      <Stagger in>
      <div 
        // onClick={this.setState({ urlLink:"https://www.youtube.com/watch?v=LEi_dT9KDJI"})}
       
        // onClick={this.toggleVideoPlayer}
        >
          <Row>
             {this.listComments}
          </Row>
     
      </div>
      </Stagger>
      {/* <CommentForm dishId={dishId} postComment={postComment} /> */}
      <Modal className='modal-lg modalStyle'
      isOpen={this.state.isVideoPlayerOpen} 
              toggle={this.toggleVideoPlayer}
              autoFocus={true} >
                 <Breadcrumb>
      <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
      {/* <BreadcrumbItem><Link to={`/menu/${dish.id}`} >sdjl}</Link></BreadcrumbItem> */}
        <BreadcrumbItem active>Video</BreadcrumbItem>
  </Breadcrumb>
          <div className='container row'>
            <div className="col-md-8">
              <VideoPlayer urlLink={this.state.urlLink} />
            </div>
            <div className="col-md-4 justify-content-end">
      {this.listComments}
      </div>
            </div>   
        
        </Modal>
    </div>
  );
  }
  
}

function RenderDish({ dish }) {
  return (
    <div>
      <FadeTransform in transformProps={{
                exitTransform: 'scale(0.5) translate(-50%)'
            }}>
               <div className='backButton'>
        <Link to={`/menu`} >
            <div className = "back-btn"><span className="fa fa-arrow-left fa-lg"></span></div>
          </Link>
        </div>
      <Card className="transparentCard">
        <CardImg width="100%" src={baseUrl + dish.image} alt='image' />
            
        {/* <CardTitle> {dish.name} </CardTitle>
        <CardBody> {dish.description} </CardBody> */}
      </Card>
      </FadeTransform>
    </div>
  )
}


const DishDetail = (props) => {

  if(props.isLoading){
    return(
      <div className="container fullPage">
        <div className="row">
          <Loading />
        </div>
      </div>
    )
  }
  else if (props.errMess){
    return(
      <div className="container fullPage">
        <div className="row">
          <h4>{props.errMess}</h4>
        </div>
      </div>
    )
  }
  else if (props.dish != null)
  return (
    <div className="container-fluid fullPage">
      <div>
       
        {/* <div className="col-12 col-md-12 m-1 ">
          <Breadcrumb>
            <BreadcrumbItem> <Link to='/home'>Home</Link></BreadcrumbItem>
            <BreadcrumbItem> <Link to='/menu'>Menu</Link></BreadcrumbItem>
            <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
          </Breadcrumb>
        </div> */}
      </div>
      <div className="container-fluid">
        
        <Row>
          <Col md={3}>
            <div className="">
             <RenderDish dish={props.dish} />
           </div> 
          </Col>
        
          
          <Col md={9}>
             <RenderComments comments={props.comments}
                            postComment={props.postComment} 
                            dishId={props.dish.id}
                            imagePath={props.dish.image}
            /> 
          </Col>
            
             
        </Row>
 </div>

    </div>
  );
}


export default DishDetail;