import React , { Component } from "react";
import ReactPlayer from "react-player";
import { Button, Breadcrumb, BreadcrumbItem, Row, Col } from 'reactstrap';
import {baseUrl} from '../shared/baseUrl';
import { Link } from 'react-router-dom';
import {  } from 'reactstrap';
import { FadeTransform,Fade, Stagger } from 'react-animation-components';


const playerStyle={
  marginBottom:10,
  padding:0,
  border:0
}
const RenderTitle = (props) => {
  return(
  <h2>{props.comments.author}</h2>
  )
}
class RenderDescription extends Component{

  listComments = this.props.comments.map((commentdetail) => {
    return (
      <div>
      <ul>
        <Fade in>
      <li key={commentdetail.id} className="">
        <Row>
          <Col md={10}>
            <div className="video-lists">
          <div className="">
          
          {commentdetail.comment}
        </div>

        <div>
          -- {commentdetail.author}, {commentdetail.date}
          <p></p>
        </div>
        </div>
          </Col>
        </Row>
        
      </li>
      </Fade>
      </ul>

     
      </div>
    );
  }

  );
  render(){
    return (
    <div>
      <Stagger in>
      <div 
     
        >
      {this.listComments}
      </div>
      </Stagger>
     
      
    </div>
  );
  }
  
}
class RenderComments extends Component{

  listComments = this.props.comments.map((commentdetail) => {
    return (
      <div>
      <ul>
        <Fade in>
        <Link to={`/menu/${commentdetail.dishId}/${encodeURIComponent(commentdetail.videoLink)}`} >

      <li key={commentdetail.id} className="menuItem">
        <Row>
          <Col md={2}>
          {/* <div className="playIcon ml-4">  <span className="fa bl-100 fa-play mr-1"></span></div> */}
          <div className = "play-btn ml-4 mt-3 pl-3"><span className="fa fa-play fa-lg"></span></div>
          </Col>
          <Col md={10}>
            <div className="video-lists">
          <div className="title">
          
          {commentdetail.comment}
        </div>

        <div>
          -- {commentdetail.author}
          <p></p>
        </div>
        </div>
          </Col>
        </Row>
        
      </li>
     </Link>
      </Fade>
      </ul>

     
      </div>
    );
  }

  );
  render(){
    return (
    <div>
      <Stagger in>
      <div 
     
        >
      {this.listComments}
      </div>
      </Stagger>
     
      
    </div>
  );
  }
  
}
const VideoPlayer = (props) => {

    const decodeUriComponent = require('decode-uri-component');
    var urls= decodeUriComponent(props.linkVideo);
    var url=baseUrl+urls;
    console.log('prop'+urls)
    return (
      <div className="container-fluid videoPlayerPage fullPage video-section">
       <Row>
         <Col md={6}>
          <div className="justify-content-center player-wrapper ">  
          <Link to={`/menu/${props.dishId}`} >
            <div className = "back-btn mb-5"><span className="fa fa-arrow-left fa-lg"></span>
            <RenderTitle 
             comments={props.comments.filter((comment) => comment.videoLink===urls)}/>
            </div>
          </Link>
            <ReactPlayer 
              url= {url}
              // url="youtu.be/RXXbaFSTP6Q"
              controls={true}
            
              width={720}
              height={480}
              style={playerStyle}
            className='react-player'/>
            <RenderDescription 
             comments={props.comments.filter((comment) => comment.videoLink===urls)}/>
          </div>
          </Col>
          <Col md={5}>
            <h1 className="ml-5 pl-5">Videos</h1>
          <div className="list-section">
            <RenderComments comments={props.comments} />
          </div>
          </Col>
          
      </Row>
    </div>
  )
}

export default VideoPlayer;