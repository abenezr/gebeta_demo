import React, { Component } from "react";
import Menu from './MenuComponent'
import DishDetail from './DishdetailComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Home from './HomeComponent';
import Contact from './ContactComponent';
import About from './AboutComponent';
import AudioBook from './AudioBookComponent';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {postComment, fetchDishes, fetchBooks, fetchComments, fetchPromos , fetchLeaders, postFeedback} from '../redux/ActionCreators';
import {actions} from 'react-redux-form';
import {TransitionGroup, CSSTransition} from 'react-transition-group';
import VideoPlayer from './VideoPlayer';
import reactToString from 'react-to-string';

const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        books:state.books,
        comments: state.comments,
        promotions: state.promotions,
        leaders: state.leaders,
        feedback:state.feedback
    }
}

const mapDispatchToProps=(dispatch) => ({
    postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment)),
    fetchDishes:() => {dispatch(fetchDishes())},
    fetchBooks:() => {dispatch(fetchBooks())},
    resetFeedbackForm: () => {dispatch(actions.reset('feedback'))},
    fetchComments:() => {dispatch(fetchComments())},
    fetchPromos: () => dispatch(fetchPromos()),
    fetchLeaders:() => dispatch(fetchLeaders()),
    postFeedback:(firstname, lastname,  telnum, email, agree, contactType, message) => dispatch(postFeedback(firstname, lastname,  telnum, email, agree, contactType, message))
});

class Main extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount(){
        this.props.fetchDishes();
        this.props.fetchBooks();
        this.props.fetchComments();
        this.props.fetchPromos();
        this.props.fetchLeaders();
    }

    render() {
        const HomePage = () => {
            return (
                <Home 
                dish={this.props.dishes.dishes.filter((dish) => dish.featured)[0]}
                dishesLoading={this.props.dishes.isLoading}
                dishErrMess={this.props.dishes.errMess}
                promotion={this.props.promotions.promotions.filter((promo) => promo.featured)[0]}
                promoLoading={this.props.promotions.isLoading}
                promoErrMess={this.props.promotions.errMess}
                leader={this.props.leaders.leaders.filter((leader) => leader.featured)[0]}
                leadersLoading={this.props.leaders.isLoading}
                leadersErrMess={this.props.leaders.errMess}
            />
            );
        }

        const AboutUsPage = () => {
            return (
                <About leaders={this.props.leaders} />
            )
        }
       
        const VideoPlayerPage =({ match }) => {
            return(
                
                <VideoPlayer
                linkVideo={match.params.videoLink}
                dishId={match.params.dishId}
                dish={this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishId, 10))[0]}
                isLoading ={this.props.dishes.isLoading}
                    errMess= {this.props.dishes.errMess}
                    comments={this.props.comments.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId, 10))}
                    commentsErrMess= {this.props.comments.errMess}
                />
            )
        }
        const AudioBookPage=({match}) => {
            return(
                <AudioBook 
                    book={this.props.books.books.filter((book) => book.id=== parseInt(match.params.bookId,10))[0]}
                    isLoading={this.props.books.isLoading}
                    errMess={this.props.books.errMess}
                />
            )
        }
        const DishWithId = ({ match }) => {
            return (
                <DishDetail dish={this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishId, 10))[0]}
                isLoading ={this.props.dishes.isLoading}
                    errMess= {this.props.dishes.errMess}
                    comments={this.props.comments.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId, 10))}
                    commentsErrMess= {this.props.comments.errMess}
                    postComment={this.props.postComment}
                />
            );
        }
        return (
            <div>
                {/* <Header /> */}
                <TransitionGroup>
                    <CSSTransition key={this.props.location.key} classNames="page" timeout={300}>
                <Switch>
                    <Route path='/home' component={HomePage} />
                    <Route exact path="/menu" component={() => <Menu dishes={this.props.dishes} books={this.props.books}/>} />
                    <Route exact path="/menu/:dishId" component={DishWithId} />
                    <Route path="/menu/:dishId/:videoLink" component={VideoPlayerPage} />
                    <Route exact path="/contactus" component={ ()=> 
                    <Contact resetFeedbackForm={this.props.resetFeedbackForm} 
                            postFeedback={this.props.postFeedback} />} />
                    <Route path="/aboutus" component={AboutUsPage} />
                    <Route path="/books/:bookId" component={AudioBookPage} />
                    <Route path="/video" component={VideoPlayerPage} />
                    <Redirect to="/menu" />
                </Switch>
                </CSSTransition>
                </TransitionGroup>
                {/* <Footer /> */}
            </div>


        );
    }
}
export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Main));