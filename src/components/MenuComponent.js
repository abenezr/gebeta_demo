import React, { useState } from "react";
import {
  Card,
  CardImg,
  CardImgOverlay,
  CardTitle,
  Breadcrumb,
  BreadcrumbItem,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Button,
  CardText,
  Row,
  Col,
  CardBody,
} from "reactstrap";
import { Link } from "react-router-dom";
import { Loading } from "./LoadingComponent";
import { baseUrl } from "../shared/baseUrl";
import classnames from "classnames";
import RenderReport from './RenderReport'
import RenderReport2 from './RenderReport2'

function RenderMenuItem({ dish }) {
  console.log(baseUrl + dish.image);
  return (
    <Card className="menuCardStyle menuItem">
      <Link to={`/menu/${dish.id}`}>
        <CardImg
          className=""
          width="95%"
          src={baseUrl + dish.image}
          alt={dish.name}
        />
        <CardImgOverlay>
          <h6 className="">{dish.name}</h6>
        </CardImgOverlay>
        {/* <CardBody className="menuCardStyle">
					<CardTitle> {dish.name}</CardTitle>
				</CardBody> */}
      </Link>
    </Card>
  );
}
function RenderBookItem({ book }) {
  return (
    <Card className=" menuCardStyle menuItem ">
      <Link to={`/books/${book.id}`}>
        <CardImg
          className="menuCardStyle "
          width="90%"
          src={baseUrl + book.image}
          alt={book.name}
        />

        <CardBody className="bookBackground">
          <CardText className="booksList">
            <span className="fa fa-book fa-lg"></span> {book.name}
          </CardText>
        </CardBody>
      </Link>
    </Card>
  );
}

function RenderLogo() {
  return (
    <div>
      <Card className="homeLogo">
        <CardImg className="homeLogo" src={baseUrl + "images/ahoon.png"} />
      </Card>
    </div>
  );
}

const Menu = (props) => {
  const [activeTab, setActiveTab] = useState("1");

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  const menu = props.dishes.dishes.map((dish) => {
    return (
      <Col key={dish.id} md={4}>
        <RenderMenuItem dish={dish} />
      </Col>
    );
  });
  const book = props.books.books.map((book) => {
    return (
      <Col key={book.id} md={4}>
        <RenderBookItem book={book} />
      </Col>
    );
  });
  const RenderTab = () => {
    return (
		<Row>
		<Col md={4}>
		  <Row className="fixed-logo-place">
			<Col md={8} className="logoPlace">
			  <RenderLogo />
			</Col>
			<Col md={4} className="navTabLinks"></Col>
		  </Row>
		</Col>
		<Col md={8}>
		<RenderReport />>
		</Col>
	  </Row>
    );
  };

  if (props.dishes.isLoading) {
    return (
      <div className="container">
        <div className="row">
          <Loading />
        </div>
      </div>
    );
  } else if (props.dishes.errMess) {
    return (
      <div className="container">
        <div className="row">
          <h4>{props.dishes.errMess}</h4>
        </div>
      </div>
    );
  } else
    return (
      <div className="fullPage">
        <div className="container">
          <div className="row">
            <Nav tabs className="mt-5 justify-content-center">
              <NavItem className="tabCategory">
                <NavLink
                  className="tabTitle"
                  className={classnames({ active: activeTab === "1" })}
                  onClick={() => {
                    toggle("1");
                  }}
                >
                  Channels
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className="tabTitle"
                  className={classnames({ active: activeTab === "2" })}
                  onClick={() => {
                    toggle("2");
                  }}
                >
                  Library
                </NavLink>
              </NavItem>
              <NavItem className="tabCategory">
                <NavLink
                  className="tabTitle"
                  className={classnames({ active: activeTab === "3" })}
                  onClick={() => {
                    toggle("3");
                  }}
                >
                  Reports
                </NavLink>
              </NavItem>
            </Nav>
          </div>
        </div>
        <div className="container-fluid">
          <Row>
            {/* <Col md={4}>
						<Row className="fixed-logo-place">
							<Col md={8} className="logoPlace">
							<RenderLogo />
							</Col>
							<Col md={4} className="navTabLinks">
							
							</Col>
				
						</Row>
						
		
					</Col> */}
            <Col md={12}>
              <TabContent activeTab={activeTab}>
                <TabPane tabId="1">
                  <div className="container-fluid menuContainer">
                    <Row>
                      <Col md={4}>
                        <Row className="fixed-logo-place">
                          <Col md={8} className="logoPlace">
                            <RenderLogo />
                          </Col>
                          <Col md={4} className="navTabLinks"></Col>
                        </Row>
                      </Col>
                      <Col md={8}>
                        <Row>{menu}</Row>
                      </Col>
                    </Row>
                  </div>
                </TabPane>
                <TabPane tabId="2">
                  <div className="container-fluid menuContainer">
                    <Row>
                      <Col md={4}>
                        <Row className="fixed-logo-place">
                          <Col md={8} className="logoPlace">
                            <RenderLogo />
                          </Col>
                          <Col md={4} className="navTabLinks"></Col>
                        </Row>
                      </Col>
                      <Col md={8}>
                        <Row>{book}</Row>
                      </Col>
                    </Row>
                  </div>
                </TabPane>
                <TabPane tabId="3">
                    
                      <RenderReport />
                   
           
                </TabPane>
              </TabContent>
            </Col>
          </Row>
        </div>
      </div>
    );
};

export default Menu;
