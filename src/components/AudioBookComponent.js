import React, {Component} from 'react';
import  {Loading} from './LoadingComponent';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Media,Row, Col , CardImg} from 'reactstrap';
import { Link } from 'react-router-dom';
import {baseUrl} from '../shared/baseUrl';
import { bool } from 'prop-types';

function RenderBook({book}) {
    return (
        <div className="container-fluid">
            <div className="mt-2 pt-5 pb-2">
            <Row> 
                <Col md={2}>
                <Link to={`/menu`} >
            <div className = "back-btn"><span className="fa fa-arrow-left fa-lg"></span></div>
          </Link>
                </Col>
                <Col md={10}>
                <h3 className="ml-1 pl-1 book-title">{book.name}</h3>
                </Col>
                
    
                    <hr />
                
            </Row>
            </div>
            
            <div className="row row-content bookBackground">
                <div className="col-12 col-md-6  pl-5 pt-5">
                 {book.description}
                 {book.description}
                 {book.description}
                 {book.description}
                 {book.description}
                 {book.description}
                 {book.description}
                 </div>
                <div className="col-12 col-md-5 mt-5">
                    <Card>
                        <CardHeader className="bg-primary text-white">Facts At a Glance</CardHeader>
                        <CardBody>
                            <dl className="row p-1">
                                <dt className="col-6">Title</dt>
                                <dd className="col-6">{book.name}</dd>
                                <dt className="col-6">Category</dt>
                                <dd className="col-6">{book.category}</dd>
                                <dt className="col-6">Author</dt>
                                <dd className="col-6">{book.author}</dd>
                                <dt className="col-6">Date</dt>
                                <dd className="col-6">{book.date}</dd>
                            </dl>
                        </CardBody>
                    </Card>
                </div>
                <div className="col-12 mt-3">
                    <Card>
                        <CardBody className="bg-faded">
                            <blockquote className="blockquote pt-2 ">
                            <div className="col-12 col-md-12 pl-5 ">
                 
                 <Row>
                 <Col md={7}>
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                </Col>
                <Col md={3}>
                <CardImg className="pdf-card" width="90%" src={baseUrl + book.image} alt={book.name} />

                </Col>
                </Row>
                <div>
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                </div>
                <div>
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                </div>
                <div>
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                </div>
                <div>
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                </div>
                 
                 </div>
                 <div className="col-12 col-md-12 pl-5 ">
                 
                 <Row>
                   <Col md={5}>
                <CardImg className="pdf-card" width="90%" src={baseUrl + book.image} alt={book.name} />

                </Col>
                 <Col md={7}>
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                </Col>
                
                </Row>
                <div>
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                </div>
                <div>
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                </div>
                <div>
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                </div>
                <div>
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                     {book.description}
                </div>
                 
                 </div>
                
                                <footer className="blockquote-footer">Yogi Berra,
                        <cite title="Source Title">The Wit and Wisdom of Yogi Berra,
                            P. Pepe, Diversion Books, 2014</cite>
                                </footer>
                            </blockquote>
                        </CardBody>
                    </Card>
                </div>
            </div>
            <div className="row row-content">
                <div className="col-12">
                    <h2></h2>
                </div>
                <div className="col-12">

                </div>
            </div>
        </div>
    )
}
const AudioBook = (props) => {

    if(props.isLoading){
      return(
        <div className="container fullPage">
          <div className="row">
            <Loading />
          </div>
        </div>
      )
    }
    else if (props.errMess){
      return(
        <div className="container fullPage">
          <div className="row">
            <h4>{props.errMess}</h4>
          </div>
        </div>
      )
    }
    else if (props.book != null)
    return (
      <div className="container-fluid fullPage">
          <RenderBook book={props.book} />
   </div>
  
    );
  }

export default AudioBook;